<?php

namespace Lampaweb\Positions\Controllers;

use Illuminate\Routing\Controller;

class PositionsController extends Controller
{
    
    public function postSetPositions()
    {

        print_r(\Input::get('sorted_ids'));


        foreach (\Input::get('sorted_ids') as $orderValue => $id) {

            if (!ctype_digit($id) && !is_int($id)) {
                continue;
            }
            \DB::table(\Input::get('table'))->where('id', $id)->update([ 'position' => ($orderValue + 1) ]);
        }
    }
}
