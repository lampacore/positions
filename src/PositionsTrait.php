<?php

namespace Lampaweb\Positions;

use Illuminate\Database\Eloquent\Builder;


/**
 * Class PositionsTrait
 * @package Lampaweb\Positions
 * @method Builder positionOrder()
 * @method Builder posOrd()
 */
trait PositionsTrait
{
    
    public static function setPositions(array $sortedIdArr)
    {

        foreach ($sortedIdArr as $orderValue => $id) {

            if (!ctype_digit($id) && !is_int($id)) {
                continue;
            }

            static::where('id', $id)->update([ 'position' => ($orderValue + 1) ]);

        }
        
    }
    
    public function scopePositionOrder($q)
    {
        $q->orderBy('position', 'asc')->orderBy('id', 'desc');

        return $q;
    }

    public function scopePosOrd($q)
    {
        return $this->scopePositionOrder($q);
    }
    
}
