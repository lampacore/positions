$(function() {

    var axis = $("#sortable-items-container").attr('axis');

    $("#sortable-items-container").sortable({

        handle: ".item-drag",
        cursor: "move",
        axis: axis,
        revert: 100,

        update: function(event, ui) {

            //alert(1);

            var sortedArr = $("#sortable-items-container").sortable("toArray", {
                attribute: "object-id"
            });

           // alert(sortedArr);

            $.post("/admin/positions/set-positions", {sorted_ids: sortedArr, table: tableForSort}, function (answer) {

                }).fail(function (x) { alert(x.responseText)});


        }


    });
        

})
